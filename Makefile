# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: alex <alex@student.unit.ua>                +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2016/11/29 11:15:59 by akaplyar          #+#    #+#              #
#    Updated: 2019/02/24 12:37:09 by alex             ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#------------------------------- Echo colors -----------------------------------

ESC = "\033["
GREEN = "$(ESC)32m"
RED =   "$(ESC)31m"
EOC =   "$(ESC)0m"

#------------------------------- General Variables -----------------------------

CC = @gcc
CFLAGS = -Wall -Wextra -Werror

OBJ_PATH  =		obj/
HEADER_PATH =	includes/
HEADER_LIST =	char.h int.h libft.h list.h memory.h metaheader.h\
				printers.h state.h strings.h
HEADERS =		$(addprefix ${HEADER_PATH},${HEADER_LIST})


#------------------------------- Source pathes ---------------------------------
PRINT_PATH =	printers/ printers/ft_printf/

LISTS_PATHES =	list/ liter/ lmeta/
LISTS_PATH =	$(addprefix lists/,${LISTS_PATHES})

STRING_PATHES =	altering/ comparison/ conversion/ creation/ deletion/\
				iterating/ searching/
STRING_PATH =	$(addprefix string/,${STRING_PATHES})

SRC_PATH =		int/ char/ memory/ reader/ state/ matrix/ functions/\
				${LISTS_PATH} ${PRINT_PATH} ${STRING_PATH}

vpath %.h ${HEADER_PATH}
vpath %.c ${SRC_PATH}

#------------------------------- Object files ----------------------------------

FNS_O =		idfn nop

READ_O =	get_next_line ft_readfile

MATRIX_O =	ft_matrix_count ft_matrix_del

INT_O =		ft_int_sort ft_intlen ft_atoi ft_atoi_base ft_bit_swap

LIST_O =	list_count list_del list_filter list_find list_fold list_iter\
			list_link list_map list_new list_remove list_to_array

LITER_O =	liter_count liter_del liter_filter liter_find liter_fold liter_iter\
			liter_link liter_map liter_new liter_remove liter_to_array

LMETA_O =	lmeta_count lmeta_del lmeta_filter lmeta_find lmeta_fold lmeta_iter\
			lmeta_link lmeta_map lmeta_new lmeta_remove lmeta_to_array

LISTS_O =	${LIST_O} ${LITER_O} ${LMETA_O}

CHAR_O =	ft_toupper ft_tolower ft_isprint ft_isalnum ft_isalpha ft_isascii\
			ft_isdigit ft_isspace

MEM_O =		ft_bzero ft_memalloc ft_memccpy ft_memchr ft_memcmp ft_memcpy\
			ft_memmove ft_memset ft_ptr_free


STR_O =		ft_strchr ft_strcount ft_strnstr ft_strrchr ft_strstr\
			ft_strcat ft_strclr ft_strcpy ft_strncat ft_strncpy ft_strrev\
			ft_pathjoin ft_strdup ft_strjoin ft_strmake ft_strnew ft_strsub\
			ft_strtrim ft_str_upper ft_strcmp ft_strequ ft_llitoa ft_strsplit\
			ft_str_free ft_strisdigit ft_striter ft_strlen ft_strmap ft_strmapi\
			ft_strcontains ft_str_start_with ft_strglue

PRINT_O =	ft_putstr ft_putnbr ft_putendl ft_putchar ft_write\
			finder finder_2 ft_printf ft_vsprintf helper help_int\
			parse_brackets parse_int parse_str parse_wstr

STATE_O =	ft_exit ft_filter ft_handle_storage ft_malloc ft_debug

OBJS =		${MATRIX_O} ${INT_O} ${LISTS_O} ${CHAR_O} ${MEM_O} ${STR_O}\
			${READ_O} ${PRINT_O} ${STATE_O} ${FNS_O}
PREF_OBJ =	$(addprefix ${OBJ_PATH},${OBJS})
OBJ = 		$(addsuffix .o,${PREF_OBJ})

#------------------------------- Main part -------------------------------------

NAME = libft.a

all: $(NAME)

$(NAME): $(OBJ)
	@ar -rcs $(NAME) $(OBJ)
	@ranlib $(NAME)
	@echo "$(GREEN)\nLibft created$(EOC)"

$(OBJ_PATH):
	@mkdir -p $(OBJ_PATH)

$(OBJ_PATH)%.o: %.c $(HEADERS)
	@printf "$(GREEN)Compile %s$(EOC)                                \r" $<
	@$(CC) $(CFLAGS) -I$(HEADER_PATH) -c $<  -o $@

clean:
	@rm -rf $(OBJ)
	@echo "$(RED)Libft objects cleared$(EOC)"

fclean: clean
	@rm -rf $(NAME)
	@echo "$(RED)Libft deleted$(EOC)"

re: fclean all

.PHONY: all clean fclean re
