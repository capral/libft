/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   state.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alex <alex@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/10 00:38:58 by alex             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STATE_H
# define STATE_H

# include "metaheader.h"
# include "printers.h"
# include "strings.h"
# include "memory.h"

# define SET 1
# define GET 2
# define S_DATA 4
# define S_CHAR 8
# define S_STR 16

# define SET_PROJECT(name) (ft_handle_project((name), SET))
# define GET_PROJECT() (ft_handle_project(NULL, GET))

# define SAVE_DATA(d) (ft_handle_storage(&(d), NULL, NULL, SET | S_DATA))
# define SAVE_CHAR(c) (ft_handle_storage(NULL, &(c), NULL, SET | S_CHAR))
# define SAVE_STR(s) (ft_handle_storage(NULL, NULL, &(s), SET | S_STR))

# define GET_DATA(d) (ft_handle_storage(&(d), NULL, NULL, GET | S_DATA))
# define GET_CHAR(c) (ft_handle_storage(NULL, &(c), NULL, GET | S_CHAR))
# define GET_STR(s) (ft_handle_storage(NULL, NULL, &(s), GET | S_STR))

# define INIT_CHAR_FILTER(c) (SAVE_CHAR(c))
# define INIT_STR_FILTER(s) (SAVE_STR(s))

# define DEBUG_ON() (ft_debug_storage(TRUE, SET))
# define SET_DEBUG(b) (ft_debug_storage(b, SET))

/*
**	------------------------- System handling functions ------------------------
*/
void		*ft_malloc(size_t size);
void		ft_exit(const int errno);
void		ft_exit_msg(const char *msg);

/*
**	------------------------- Functional filter functions ----------------------
**	uses storage variables inside, need to call initializer macro before use
**	made for using with list_filter and others functional features
*/
t_bool		ft_char_filter(char c);
t_bool		ft_str_filter(char *str);

/*
**	------------------------ State handling functions --------------------------
**	--------------------- (uses static variables inside) -----------------------
**	handle project espects read-only string and won't free saved data
**	handle storage espects allocated data | string
**							& and will auto free on every save call
*/
char		*ft_handle_project(char *name, int mode);
void		ft_handle_storage(void **data, char *c, char **str, int mode);
int			ft_debug(const char *format, ...);
t_bool		ft_debug_storage(t_bool state, int mode);

#endif
