/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft.h                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:33:39 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_H
# define LIBFT_H

# include "char.h"
# include "int.h"
# include "libft.h"
# include "list.h"
# include "memory.h"
# include "metaheader.h"
# include "printers.h"
# include "state.h"
# include "strings.h"

# define BUFF_SIZE 100

/*
**	--------------------------- Read functions ---------------------------------
*/
int					get_next_line(int const fd, char **line);
char				*ft_readfile(const int fd);

/*
**	--------------------------- Matrix functions -------------------------------
*/
int					ft_matrix_count(void **matrix);
void				ft_matrix_del(void **matrix, int count);

#endif
