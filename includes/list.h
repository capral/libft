/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list.h                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alex <alex@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 19:41:13 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/10 20:54:02 by alex             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIST_H
# define LIST_H

# include "libft.h"

/*
**	each struct's API consist minimum of: creation, deletion, linking,
**	operational features: count, map, iter, find, filter, remove, fold
**	conversion: array,
*/
typedef struct s_list	t_list;
typedef struct s_liter	t_liter;
typedef struct s_lmeta	t_lmeta;
typedef struct s_dlist	t_dlist;

/*
**		List
*/
struct			s_list
{
	void		*data;
	t_list		*next;
};

t_list			*list_new(void *data);
t_list			*list_new_cpy(void const *data, size_t size);

void			list_del(t_list **root);
void			list_del1(t_list **root);
void			list_del_fn(t_list **root, t_nop_fn1 f);
void			list_del1_fn(t_list **root, t_nop_fn1 f);

void			list_add(t_list **root, t_list *new_node);
void			list_push(t_list **root, t_list *new_node);

size_t			list_count(t_list *list);
t_list			*list_map(t_list *list, t_fn1 f);
void			list_iter(t_list *list, t_nop_fn1 f);
t_list			*list_find(t_list *list, t_pred_fn1 f);
t_list			*list_filter(t_list *list, t_pred_fn1 f);
t_list			*list_filter_cpy(t_list *list, t_pred_fn1 f, t_fn1 g);
void			list_remove(t_list *list, t_pred_fn1 f);
void			list_remove_fn(t_list *list, t_pred_fn1 f, t_nop_fn1 g);
void			*list_fold(t_list *list, void *acc, t_fn2 f);

void			**list_to_array(t_list *list);
void			**list_to_array_fn(t_list *list, t_fn1 f);
/*
**		List iterator
*/
struct			s_liter
{
	t_list		*prev;
	t_list		*this;
};

t_liter			*liter_new(void);
t_liter			*liter_new_init(t_list *list);

void			liter_free(t_liter **iter);
void			liter_del(t_liter *iter);
void			liter_delr(t_liter *iter, t_list **root);
void			liter_del_fn(t_liter *iter, t_nop_fn1 f);
void			liter_delr_fn(t_liter *iter, t_list **root, t_nop_fn1 f);

void			liter_add(t_liter *iter, t_list *new_node);
void			liter_push(t_liter *iter, t_list *new_node);

size_t			liter_count(t_liter *iter);
t_list			*liter_map(t_liter *iter, t_fn1 f);
void			liter_iter(t_liter *iter, t_nop_fn1 f);
t_liter			*liter_find(t_liter *iter, t_pred_fn1 f);
t_liter			*liter_find_new(t_list *list, t_pred_fn1 f);
t_list			*liter_filter(t_liter *iter, t_pred_fn1 f);
t_list			*liter_filter_cpy(t_liter *iter, t_pred_fn1 f, t_fn1 g);
void			liter_remove(t_liter *iter, t_pred_fn1 f);
void			liter_remove_fn(t_liter *iter, t_pred_fn1 f, t_nop_fn1 g);
void			*liter_fold(t_liter *iter, void *acc, t_fn2 f);

void			**liter_to_array(t_liter *iter);
void			**liter_to_array_fn(t_liter *iter, t_fn1 f);
/*
**		Meta list with some metadata about list
*/
struct			s_lmeta
{
	int			count;
	t_list		*first;
	t_list		*last;
	t_liter		*iter;
};

t_lmeta			*lmeta_new(void);
t_lmeta			*lmeta_new_init(t_list *list);

void			lmeta_del(t_lmeta *meta);
void			lmeta_del_fn(t_lmeta *meta, t_nop_fn1 f);
void			lmeta_free(t_lmeta **meta, t_nop_fn1 f, t_bool all);

void			lmeta_add(t_lmeta *meta, t_list *new_node);
void			lmeta_add_data(t_lmeta *meta, void *data);
void			lmeta_push(t_lmeta *meta, t_list *new_node);
void			lmeta_push_data(t_lmeta *meta, void *data);

size_t			lmeta_count(t_lmeta *meta);
t_lmeta			*lmeta_map(t_lmeta *meta, t_fn1 f);
void			lmeta_iter(t_lmeta *meta, t_nop_fn1 f);
t_liter			*lmeta_find(t_lmeta *meta, t_pred_fn1 f);
t_lmeta			*lmeta_filter(t_lmeta *meta, t_pred_fn1 f);
t_lmeta			*lmeta_filter_cpy(t_lmeta *meta, t_pred_fn1 f, t_fn1 g);
void			lmeta_remove(t_lmeta *meta, t_pred_fn1 f);
void			lmeta_remove_fn(t_lmeta *meta, t_pred_fn1 f, t_nop_fn1 g);
void			*lmeta_fold(t_lmeta *meta, void *acc, t_fn2 f);

void			**lmeta_to_array(t_lmeta *meta);
char			**lmeta_to_char_array(t_lmeta *meta);
void			**lmeta_to_array_fn(t_lmeta *meta, t_fn1 f);

/*
**		Double-linked List
*/
struct			s_dlist
{
	void		*data;
	t_dlist		*next;
	t_dlist		*prev;
};

t_dlist			*dlist_new(void *data);
t_dlist			*dlist_new_cpy(void const *data, size_t size);

void			dlist_del(t_dlist **root);
void			dlist_del1(t_dlist **root);
void			dlist_del_fn(t_dlist **root, t_nop_fn1 f);
void			dlist_del1_fn(t_dlist **root, t_nop_fn1 f);

void			dlist_add(t_dlist **root, t_dlist *new_node);
void			dlist_push(t_dlist **root, t_dlist *new_node);
void			dlist_add_data(t_dlist *dlist, void *data);
void			dlist_push_data(t_dlist *dlist, void *data);

size_t			dlist_count(t_dlist *dlist);
// t_dlist			*dlist_map(t_dlist *dlist, t_fn1 f);
void			dlist_iter(t_dlist *dlist, t_nop_fn1 f);
t_dlist			*dlist_find(t_dlist *dlist, t_pred_fn1 f);
// t_dlist			*dlist_filter(t_dlist *dlist, t_pred_fn1 f);
// t_dlist			*dlist_filter_cpy(t_dlist *dlist, t_pred_fn1 f, t_fn1 g);
// void			dlist_remove(t_dlist *dlist, t_pred_fn1 f);
// void			dlist_remove_fn(t_dlist *dlist, t_pred_fn1 f, t_nop_fn1 g);
void			*dlist_fold(t_dlist *dlist, void *acc, t_fn2 f);

void			**dlist_to_array(t_dlist *dlist);
void			**dlist_to_array_fn(t_dlist *dlist, t_fn1 f);

#endif
