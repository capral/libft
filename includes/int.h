/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   int.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:32:49 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef INT_H
# define INT_H

# include "strings.h"

int					ft_atoi(const char *str);
int					ft_atoi_base(const char *str, int base);
int					ft_intlen(long long num);
void				ft_int_sort(int *tab, size_t size);
unsigned			ft_bit_swap(unsigned u);
unsigned long long	ft_bit_swap_ll(unsigned long long llu);
#endif
