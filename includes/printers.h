/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   printers.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:33:03 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PRINTERS_H
# define PRINTERS_H

# include "int.h"
# include "state.h"
# include <stdarg.h>

/*
**	-------------------------- Print primitives --------------------------------
*/
ssize_t		ft_write(int fd, const void *buffer, size_t size);

void		ft_putchar(char c);
void		ft_putchar_fd(char c, int fd);

void		ft_putstr(const char *s);
void		ft_putstr_fd(const char *s, int fd);

void		ft_putendl(const char *s);
void		ft_putendl_fd(const char *s, int fd);

void		ft_putnbr(int n);
void		ft_putnbr_fd(int n, int fd);

/*
**	------------------------ Printf-family functions ---------------------------
*/
int			ft_printf(const char *format, ...);
int			ft_dprintf(int fd, const char *format, ...);
int			ft_sprintf(char **str, const char *format, ...);
int			ft_vsprintf(char **str, const char *format, va_list ap);

#endif
