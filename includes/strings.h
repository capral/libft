/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   strings.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 20:31:21 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:33:17 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef STRINGS_H
# define STRINGS_H

# include "libft.h"

/*
**	--------------------------- creation ---------------------------------------
*/
char		*ft_strnew(size_t size);
char		*ft_strdup(const char *s1);
char		*ft_strndup(const char *src, size_t len);
char		*ft_strjoin(char *s1, char *s2);
char		*ft_pathjoin(char *dir, char *file);
char		*ft_strmake(char *s1, char *s2, int del);

char		*ft_strglue(char *sep, char **list);
char		*ft_strglue2(char *sep, char **list);
char		*ft_strglue_pref(char *pref, char *sep, char **list);

char		*ft_strsub(const char *s, unsigned int start, size_t len);

/*
**	--------------------------- deletion ---------------------------------------
*/
void		ft_str_free(char **ptr, char *next);

/*
**	--------------------------- searching --------------------------------------
*/
int			ft_strcount(const char *str, char c);
char		*ft_strchr(const char *s, int c);
char		*ft_strrchr(const char *s, int c);
char		*ft_strchr_space(const char *s);
char		*ft_strstr(const char *big, const char *little);
char		*ft_strnstr(const char *big, const char *little, size_t len);
t_bool		ft_str_start_with(const char *str, const char *look);

/*
**	--------------------------- altering ---------------------------------------
*/
void		ft_strclr(char *s);
void		ft_str_upper(char *str);
char		*ft_strrev(char *str);
char		*ft_strtrim(const char *s);
char		*ft_strcpy(char *dst, const char *src);
char		*ft_strncpy(char *dst, const char *src, size_t len);
char		*ft_strcat(char *restrict s1, const char *restrict s2);
char		*ft_strncat(char *restrict s1, const char *restrict s2, size_t n);

/*
**	--------------------------- comparison -------------------------------------
*/
int			ft_strcmp(const char *s1, const char *s2);
int			ft_strncmp(const char *s1, const char *s2, size_t n);
int			ft_strequ(const char *s1, const char *s2);
int			ft_strnequ(const char *s1, const char *s2, size_t n);

/*
**	--------------------------- conversion -------------------------------------
*/
char		*ft_itoa(int value);
char		*ft_llitoa(intmax_t value);
char		*ft_llitoa_base(uintmax_t value, int base);
char		**ft_strsplit(const char *s, char c);

/*
**	--------------------------- iterating --------------------------------------
*/
size_t		ft_strlen(const char *s);
void		ft_striter(char *s, void (*f)(char *));
void		ft_striteri(char *s, void (*f)(unsigned int, char *));
int			ft_strisdigit(const char *str);
char		*ft_strmap(char const *s, char (*f)(char));
char		*ft_strmapi(char const *s, char (*f)(unsigned int, char));
t_bool		ft_strcontains(char *str, t_pred_fn1 f);

#endif
