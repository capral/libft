/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   metaheader.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:33:01 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef METAHEADER_H
# define METAHEADER_H

# include <stdlib.h>
# include <stdint.h>
# include <unistd.h>

/*
**	Special definition section
*/
# define INTMAX 0x7fffffff
# define INTMIN 0xffffffff

# define STD_IN 0
# define STD_OUT 1
# define STD_ERR 2

# define NONE 0
# define FIRST 1
# define SECOND 2
# define BOTH 4

# define ENOERR 0
# define ENOMEM 12
# define EBADWRITE 1

# define ABS(x) ((x) < 0 ? -(x) : x)
# define MAX(x, y) ((x) >= (y) ? (x) : (y))
# define MIN(x, y) ((x) < (y) ? (x) : (y))

/*
**	Boolean defition
*/
# define TRUE 1
# define FALSE 0

typedef int			t_bool;

/*
**	Functions typedefs to unify usage with list, vectors etc.
*/
typedef void		(*t_nop_fn)(void);
typedef void		(*t_nop_fn1)(void *x1);
typedef void		(*t_nop_fn2)(void *x1, void *x2);
typedef void		(*t_nop_fn3)(void *x1, void *x2, void *x3);
typedef void		(*t_nop_fn4)(void *x1, void *x2, void *x3, void *x4);

typedef void		*(*t_fn)(void);
typedef void		*(*t_fn1)(void *x1);
typedef void		*(*t_fn2)(void *x1, void *x2);
typedef void		*(*t_fn3)(void *x1, void *x2, void *x3);
typedef void		*(*t_fn4)(void *x1, void *x2, void *x3, void *x4);

typedef t_bool		(*t_pred_fn)(void);
typedef t_bool		(*t_pred_fn1)(void *x1);
typedef t_bool		(*t_pred_fn2)(void *x1, void *x2);
typedef t_bool		(*t_pred_fn3)(void *x1, void *x2, void *x3);
typedef t_bool		(*t_pred_fn4)(void *x1, void *x2, void *x3, void *x4);

typedef int			(*t_cmp_fn)(void *x1, void *x2);

/*
**	Standart functions prototypes when no action is needed
*/
void				*idfn(void *x);
void				nop(void);
void				nop1(void *x1);
void				nop2(void *x1, void *x2);
void				nop3(void *x1, void *x2, void *x3);
void				nop4(void *x1, void *x2, void *x3, void *x4);

#endif
