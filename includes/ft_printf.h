/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 12:47:35 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:33:32 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H

# include "printers.h"
# include <wchar.h>
# include <stdarg.h>

enum	e_len	{hh, h, l, ll, j, z };

enum	e_type	{d, o, O, u, U, x, X, c, C, s, S, p, r, n, b};

typedef struct	s_term
{
	char		*name;
	char		*code;
}				t_term;

typedef struct	s_form
{
	int			hash;
	int			zero;
	int			plus;
	int			minus;
	int			space;
	int			quote;
	int			dolla;
	int			width;
	int			press;
	int			len;
	int			type;
	int			sign;
	int			cl;
	char		*out;
}				t_form;

typedef struct	s_pf
{
	int			size;
	char		*out;
	t_form		*form;
	va_list		saved;
	va_list		ap;
}				t_pf;

typedef void	(*t_pffn)(t_form*, va_list);

/*
**	parse_brackets.c
*/
t_bool			pf_parse_brackets(t_pf *pf, char **format);
/*
**	parse_int.c
*/
void			pf_parse_int(t_form *form, va_list ap);
void			pf_parse_int_u(t_form *form, va_list ap);
/*
**	parse_str.c
*/
void			pf_parse_str(t_form *form, va_list ap);
void			pf_parse_non_printable(t_form *form, va_list ap);
void			pf_parse_wstr(t_form *form, va_list argc);
/*
**	finder.c
*/
void			pf_parse_percent(t_pf *pf, char **format);
void			pf_skip_va_list(t_pf *pf);
void			pf_find_dolla(char **format, t_form *form);
void			pf_find_type(t_form *form, char c);
/*
**	help_int.c
*/
void			pf_fill_hash(t_form *form);
void			pf_fill_sign(t_form *form);
void			pf_fill_press(t_form *form);
void			pf_fill_quote(t_form *form);
void			pf_fill_width(t_form *form);
/*
**	helper.c
*/
void			pf_fill_char(t_form *form);
int				pf_check_type(t_form *form);
void			pf_parse_pointer(t_pf *pr);

#endif
