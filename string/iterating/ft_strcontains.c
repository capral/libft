/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcontains.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 18:28:06 by akaplyar          #+#    #+#             */
/*   Updated: 2019/02/16 19:57:21 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

t_bool		ft_strcontains(char *str, t_pred_fn1 f)
{
	if (str && f)
	{
		while (*str && f(str))
			str++;
		return (*str ? FALSE : TRUE);
	}
	return (FALSE);
}
