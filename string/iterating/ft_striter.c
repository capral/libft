/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_striter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 11:19:42 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:10:58 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

void		ft_striter(char *s, void (*f)(char *))
{
	if (s && f)
		while (*s)
			f(&(*s++));
}

void		ft_striteri(char *s, void (*f)(unsigned int, char *))
{
	unsigned int	i;

	i = 0;
	if (s && f)
		while (s[i])
		{
			f(i, &s[i]);
			i++;
		}
}
