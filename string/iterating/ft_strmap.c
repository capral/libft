/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmap.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 11:31:38 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:10:54 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char	*ft_strmap(char const *s, char (*f)(char))
{
	char	*str;
	char	*ptr;

	if (s && f)
	{
		str = (char *)malloc(ft_strlen(s) + 1);
		if (str)
		{
			ptr = str;
			while (*s)
				*ptr++ = f(*s++);
			*ptr = '\0';
			return (str);
		}
	}
	return (NULL);
}
