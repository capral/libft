/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strequ.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 11:55:16 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:22 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

int		ft_strequ(char const *s1, char const *s2)
{
	if (!s1 || !s2)
		return (0);
	while (*s1 && *s2)
		if (*s1++ != *s2++)
			return (0);
	return (!*s1 && !*s2 ? 1 : 0);
}

int		ft_strnequ(char const *s1, char const *s2, size_t n)
{
	if (s1 && s2)
		while (*s1 && *s2 && n--)
			if (*s1++ != *s2++)
				return (0);
	return (!*s1 && !*s2 ? 1 : 0);
}
