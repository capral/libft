/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsplit.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/12/01 16:56:53 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:13 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

static size_t	count(const char *s, char c)
{
	size_t		n;

	n = 0;
	while (*s != '\0')
	{
		while (*s && *s != c)
			s++;
		if (*s && *s == c)
		{
			n++;
			s++;
		}
	}
	return (*(s - 1) == c ? n + 1 : n + 2);
}

static size_t	get_len(const char *s, char c)
{
	size_t		n;

	n = 0;
	while (s[n] && s[n] != c)
		n++;
	return (n + 1);
}

static char		**fill_tab(char **tab, const char *s, char c)
{
	int			i;
	int			j;

	i = 0;
	while (*s)
	{
		while (*s && *s == c)
			s++;
		if (*s)
		{
			j = 0;
			tab[i] = (char*)ft_malloc(sizeof(char) * get_len(s, c));
			while (*s && *s != c)
				tab[i][j++] = *s++;
			tab[i++][j] = '\0';
		}
	}
	tab[i] = 0;
	return (tab);
}

char			**ft_strsplit(const char *s, char c)
{
	char		**tab;

	if (s)
	{
		tab = (char**)ft_malloc(sizeof(char*) * count(s, c));
		return (fill_tab(tab, s, c));
	}
	else
		return (NULL);
}
