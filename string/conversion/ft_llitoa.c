/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_llitoa.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/03/13 18:46:35 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:21 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

static void		make_str_base(uintmax_t value, char *str, int base, int *i)
{
	static char	tmp[] = "0123456789abcdef";

	if (value >= (uintmax_t)base)
		make_str_base(value / base, str, base, i);
	str[(*i)++] = tmp[value % base];
}

char			*ft_llitoa(intmax_t value)
{
	int			i;
	char		*str;

	i = 0;
	if (!(str = (char*)malloc(32)))
		return (NULL);
	make_str_base(ABS(value), str, 10, &i);
	str[i] = '\0';
	return (str);
}

char			*ft_itoa(int value)
{
	int			i;
	char		*str;

	i = 0;
	if (!(str = (char*)malloc(32)))
		return (NULL);
	if (value < 0)
		str[i++] = '-';
	make_str_base(ABS(value), str, 10, &i);
	str[i] = '\0';
	return (str);
}

char			*ft_llitoa_base(uintmax_t value, int base)
{
	int			i;
	char		*str;

	i = 0;
	if (base < 2 || base > 16 || !(str = (char*)malloc(32)))
		return (NULL);
	make_str_base(value, str, base, &i);
	str[i] = '\0';
	return (str);
}
