/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncpy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 16:23:41 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:34 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char	*ft_strncpy(char *dst, const char *src, size_t n)
{
	char	*d;

	d = dst;
	while (*src)
	{
		if (!n--)
			return (d);
		*dst++ = *src++;
	}
	while (n--)
		*dst++ = '\0';
	return (d);
}
