/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strtrim.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 12:29:30 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:26 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

static size_t	get_start(const char *str)
{
	size_t		a;

	a = 0;
	while (ft_is_whitespace(str[a]))
		a++;
	return (a);
}

static size_t	get_end(const char *str)
{
	size_t		a;

	a = 0;
	a = ft_strlen(str);
	a--;
	while (ft_is_whitespace(str[a]))
		a--;
	return (++a);
}

char			*ft_strtrim(const char *s)
{
	char		*new;
	char		*ptr;
	size_t		start;
	size_t		end;

	if (!s)
		return (0);
	start = get_start(s);
	end = get_end(s);
	if (!*s || start == ft_strlen(s))
		return (ft_strnew(1));
	new = (char*)ft_malloc(end - start + 1);
	ptr = new;
	while (start < end)
		*ptr++ = s[start++];
	*ptr = '\0';
	return (new);
}
