/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strglue.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/24 11:38:00 by alex              #+#    #+#             */
/*   Updated: 2019/03/08 16:12:33 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

static void		ft_glue(char *str, char *sep, char **list, size_t *sizes)
{
	size_t	i;
	size_t	len;

	i = 0;
	len = ft_strlen(sep);
	while (list[i])
	{
		ft_strcpy(str, list[i]);
		str += sizes[i];
		if (list[i + 1])
		{
			ft_strcpy(str, sep);
			str += len;
		}
		i++;
	}
}

static size_t	*count_size(char *sep, char **list)
{
	size_t	i;
	size_t	n;
	size_t	size;
	size_t	*sizes;

	i = 0;
	sizes = NULL;
	n = ft_matrix_count((void**)list);
	if (n > 0)
	{
		size = 0;
		sizes = (size_t*)ft_memalloc(sizeof(size_t) * (n + 2));
		while (i < n)
		{
			sizes[i + 1] = ft_strlen(list[i]);
			size += sizes[++i];
		}
		if (n > 1)
			size += ft_strlen(sep) * (n / 2 + n % 2);
		sizes[0] = size + 1;
	}
	return (sizes);
}

char			*ft_strglue(char *sep, char **list)
{
	char	*str;
	size_t	n;
	size_t	size;
	size_t	*sizes;

	str = NULL;
	n = ft_matrix_count((void**)list);
	if (n == 1)
		str = ft_strdup(list[0]);
	else if (n > 1)
	{
		size = ft_strlen(sep) * (n / 2 + n % 2) + 1;
		sizes = (size_t*)ft_memalloc(sizeof(size_t) * ++n);
		while (n-- > 0)
		{
			sizes[n] = ft_strlen(list[n]);
			size += sizes[n];
		}
		str = (char*)ft_malloc(sizeof(char) * size);
		ft_glue(str, sep, list, sizes);
		free(sizes);
	}
	return (str);
}

char			*ft_strglue2(char *sep, char **list)
{
	char	*str;
	size_t	*sizes;

	str = NULL;
	sizes = count_size(sep, list);
	if (sizes)
	{
		str = (char*)ft_malloc(sizeof(char) * sizes[0]);
		ft_glue(str, sep, list, sizes + 1);
		free(sizes);
	}
	return (str);
}

char			*ft_strglue_pref(char *pref, char *sep, char **list)
{
	char	*str;
	size_t	len;
	size_t	*sizes;

	str = NULL;
	sizes = count_size(sep, list);
	if (sizes)
	{
		len = ft_strlen(pref);
		str = (char*)ft_malloc(sizeof(char) * (len + sizes[0]));
		ft_strcpy(str, pref);
		ft_glue(str + len, sep, list, sizes + 1);
		free(sizes);
	}
	return (str);
}
