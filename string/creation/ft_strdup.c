/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 15:59:08 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:11 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char		*ft_strdup(const char *s1)
{
	char	*new;

	new = (char *)ft_malloc(ft_strlen(s1) + 1);
	return (ft_strcpy(new, s1));
}

char		*ft_strndup(const char *str, size_t len)
{
	char	*new;

	new = (char*)ft_malloc(len + 1);
	ft_strncpy(new, str, len);
	new[len] = '\0';
	return (new);
}
