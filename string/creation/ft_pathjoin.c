/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_pathjoin.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alex <alex@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/12 20:34:42 by alex              #+#    #+#             */
/*   Updated: 2019/02/20 21:33:09 by alex             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char			*ft_pathjoin(char *dir, char *file)
{
	size_t		len;
	char		*path;

	if (!dir || !file)
		return (NULL);
	if (!*dir || !*file)
		return (*dir ? ft_strdup(dir) : ft_strdup(file));
	if (*file == '/')
		file++;
	len = ft_strlen(dir);
	if (dir[len - 1] == '/')
		path = ft_strjoin(dir, file);
	else
		ft_sprintf(&path, "%s/%s", dir, file);
	return (path);
}
