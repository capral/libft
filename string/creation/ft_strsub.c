/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strsub.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 12:00:40 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:02 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char	*ft_strsub(const char *s, unsigned int start, size_t len)
{
	char	*new;
	char	*ptr;

	if (!s)
		return (NULL);
	new = (char*)ft_malloc(len + 1);
	ptr = new;
	s += start;
	while (len--)
		*ptr++ = *s++;
	*ptr = '\0';
	return (new);
}
