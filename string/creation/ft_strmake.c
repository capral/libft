/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strmake.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/12 14:45:12 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:11:05 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char		*ft_strmake(char *s1, char *s2, int del)
{
	char	*str;

	str = ft_strjoin(s1, s2);
	if (s1 && del & (FIRST | BOTH))
		free(s1);
	if (s2 && del & (SECOND | BOTH))
		free(s2);
	return (str);
}
