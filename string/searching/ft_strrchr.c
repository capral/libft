/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strrchr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 16:09:01 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:10:36 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char			*ft_strrchr(const char *s, int c)
{
	const char	*str;
	char		ch;

	ch = (char)c;
	str = 0;
	while (*s)
	{
		if (*s == ch)
			str = s;
		s++;
	}
	return ((char*)(c ? str : s));
}
