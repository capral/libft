/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strstr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/29 08:47:34 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/09 12:16:26 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char	*ft_strstr(const char *big, const char *little)
{
	size_t	i;
	size_t	j;

	if (!*little)
		return ((char*)big);
	i = 0;
	while (big[i])
	{
		j = 0;
		while (little[j] == big[i + j] && little[j])
			j++;
		if (!little[j])
			return ((char*)&big[i]);
		i++;
	}
	return (NULL);
}
