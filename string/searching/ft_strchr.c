/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strchr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 17:58:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:10:48 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char		*ft_strchr_space(const char *s)
{
	char	*str;

	str = (char*)s;
	while (*str)
	{
		if (ft_isspace(*str))
			return (str);
		str++;
	}
	return (NULL);
}

char		*ft_strchr(const char *s, int c)
{
	char	*str;

	str = (char*)s;
	while (*str)
	{
		if (*str == (char)c)
			return (str);
		str++;
	}
	return (!c ? str : NULL);
}
