/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcount.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/09/17 19:46:47 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:10:45 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

int				ft_strcount(const char *str, char c)
{
	int			i;

	i = 0;
	while (*str)
		if (*str++ == c)
			i++;
	return (i);
}
