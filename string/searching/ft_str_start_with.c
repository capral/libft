/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_start_with.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/20 16:33:11 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:12:44 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

t_bool		ft_str_start_with(const char *str, const char *look)
{
	if (look)
	{
		while (*str && *look && *str == *look)
		{
			str++;
			look++;
		}
		return (*look ? FALSE : TRUE);
	}
	return (FALSE);
}
