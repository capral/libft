/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strnstr.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 16:09:02 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:10:40 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "strings.h"

char	*ft_strnstr(const char *big, const char *little, size_t len)
{
	size_t	n;
	size_t	x;

	n = ft_strlen((char*)little);
	x = 0;
	if (!*little)
		return ((char*)big);
	while (*big && x < len)
	{
		if (!ft_memcmp(big, little, n))
			if (n + x <= len)
				return ((char*)big);
		big++;
		x++;
	}
	return (0);
}
