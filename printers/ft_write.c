/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_write.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 11:38:38 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:06:28 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

ssize_t			ft_write(int fd, const void *buffer, size_t size)
{
	ssize_t		ret;
	size_t		written;

	ret = 0;
	written = 0;
	if (fd > 0)
	{
		while (written != size)
		{
			ret = write(fd, buffer + written, size - written);
			if (ret < 0)
				ft_exit(EBADWRITE);
			else
				written += ret;
		}
	}
	return (written);
}
