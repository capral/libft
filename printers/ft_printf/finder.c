/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   finder.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:05:22 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		pf_find_len(t_form *form, char **format)
{
	if (ft_strnequ(*format, "hh", 2))
		form->len = hh;
	else if (ft_strnequ(*format, "ll", 2))
		form->len = ll;
	else if (**format == 'h')
		form->len = h;
	else if (**format == 'l')
		form->len = l;
	else if (**format == 'j')
		form->len = j;
	else if (**format == 'z')
		form->len = z;
	else
		return ;
	if (form->len == l)
		form->cl = 1;
	*format += (form->len == hh || form->len == ll) ? 2 : 1;
}

static void		pf_find_press(t_pf *pf, char **format)
{
	if (**format == '.')
	{
		(*format)++;
		if (**format == '*')
		{
			pf->form->press = va_arg(pf->ap, int);
			(*format)++;
		}
		else
		{
			pf->form->press = ft_atoi(*format);
			while (ft_isdigit(**format))
				(*format)++;
		}
	}
}

static void		pf_find_width(t_pf *pf, char **format)
{
	if (**format == '*')
	{
		pf->form->width = va_arg(pf->ap, int);
		if (pf->form->width < 0)
		{
			pf->form->minus = 1;
			pf->form->width *= -1;
		}
		(*format)++;
	}
	else if (ft_isdigit(**format))
	{
		pf->form->width = ft_atoi(*format);
		while (ft_isdigit(**format))
			(*format)++;
	}
}

static void		pf_find_flags(t_form *form, char **format)
{
	if (**format == '#')
		form->hash = TRUE;
	else if (**format == '\'')
		form->quote = TRUE;
	else if (**format == ' ' && !form->plus)
		form->space = TRUE;
	else if (**format == '0' && !form->minus)
		form->zero = TRUE;
	else if (**format == '+')
	{
		form->plus = TRUE;
		form->space = FALSE;
	}
	else if (**format == '-')
	{
		form->minus = TRUE;
		form->zero = FALSE;
	}
	else
		return ;
	(*format)++;
	pf_find_flags(form, format);
}

void			pf_parse_percent(t_pf *pf, char **format)
{
	(*format)++;
	pf_find_dolla(format, pf->form);
	while (**format && ft_strchr("0123456789+-*$#.'hljz ", **format))
	{
		pf_find_flags(pf->form, format);
		pf_find_width(pf, format);
		pf_find_press(pf, format);
		pf_find_len(pf->form, format);
	}
	if (pf->form->dolla)
		pf_skip_va_list(pf);
	pf_find_type(pf->form, **format);
	if (**format)
		(*format)++;
}
