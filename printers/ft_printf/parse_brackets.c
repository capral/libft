/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_brackets.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 15:15:58 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 21:44:06 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static const t_term	g_terms[26] = {
	{"black", "30"},
	{"red", "31"},
	{"green", "32"},
	{"yellow", "33"},
	{"blue", "34"},
	{"purple", "35"},
	{"cyan", "36"},
	{"grey", "37"},
	{"^black", "40"},
	{"^red", "41"},
	{"^green", "42"},
	{"^yellow", "43"},
	{"^blue", "44"},
	{"^purple", "45"},
	{"^cyan", "46"},
	{"^grey", "47"},
	{"def", "0"},
	{"bold", "1"},
	{"half", "2"},
	{"under", "4"},
	{"blink", "5"},
	{"rev", "7"},
	{"^bold", "22"},
	{"^under", "24"},
	{"^blink", "25"},
	{"^rev", "27"}
};

void				pf_match_table(char *spec, char **ptr, int *j)
{
	int				i;

	i = 0;
	while (i < 26)
	{
		if (ft_strequ(spec, g_terms[i].name))
		{
			ptr[*(j++)] = g_terms[i].code;
			return ;
		}
		i++;
	}
}

void				pf_join_specs(t_pf *pf, char **specs)
{
	int				j;
	size_t			i;
	size_t			size;
	char			**codes;

	i = 0;
	j = 0;
	size = ft_matrix_count((void**)specs) + 1;
	codes = (char**)ft_memalloc(sizeof(char*) * size);
	while (i < size)
	{
		pf_match_table(specs[i], codes, &j);
		i++;
	}
	if (j > 0)
	{
		pf->form->out = ft_strglue_pref("\033[", ";", codes);
		pf->form->out = ft_strmake(pf->form->out, "m", FIRST);
	}
	free(codes);
}

t_bool				pf_parse_brackets(t_pf *pf, char **format)
{
	char			*ptr;
	char			*colors;
	char			**specs;

	ptr = ft_strchr(*format, '}');
	if (ptr)
	{
		colors = ft_strndup(*format + 1, ptr - *format - 2);
		specs = ft_strsplit(colors, ';');
		pf_join_specs(pf, specs);
		*format = ptr + 1;
		free(colors);
		ft_matrix_del((void**)specs, -1);
	}
	return (ptr != NULL);
}
