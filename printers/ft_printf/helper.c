/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   helper.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:04:37 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int			pf_check_type(t_form *form)
{
	int		t;

	t = form->type;
	if (t == d)
		return (0);
	else if (t >= b && t <= X)
		return (1);
	else if (t == p)
	{
		form->type = x;
		form->hash = TRUE;
		return (1);
	}
	else if ((t >= c && t <= S) || (t < 0 && *form->out))
		return (2);
	else if (t == n)
		return (3);
	else
		return (4);
}

void		pf_fill_char(t_form *form)
{
	char	*filler;

	if (--form->width > 0)
	{
		filler = ft_strnew(form->width);
		ft_memset(filler, (form->zero ? '0' : ' '), form->width);
		if (form->minus)
			form->out = ft_strmake(form->out, filler, BOTH);
		else
			form->out = ft_strmake(filler, form->out, BOTH);
	}
}

void		pf_parse_pointer(t_pf *pf)
{
	int		*ptr;

	ptr = va_arg(pf->ap, int*);
	if (ptr)
		*ptr = pf->size;
}
