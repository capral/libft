/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   finder_2.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:04:23 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		pf_skip_va_list(t_pf *pf)
{
	va_end(pf->ap);
	va_copy(pf->ap, pf->saved);
	while (--pf->form->dolla)
		va_arg(pf->ap, void *);
}

void		pf_find_dolla(char **format, t_form *form)
{
	char	*tmp;

	if (ft_isdigit(**format))
	{
		if ((form->dolla = ft_atoi(*format)))
		{
			tmp = *format;
			while (*tmp && ft_isdigit(*tmp))
				tmp++;
			if (*tmp == '$')
				*format = tmp + 1;
			else
				form->dolla = 0;
		}
	}
}

void		pf_find_type(t_form *form, char format)
{
	static const char	c_set[14] = {'o', 'O', 'u', 'U', 'x', 'X', 'c',
									'C', 's', 'S', 'b', 'n', 'p', 'r'};
	static const int	t_set[14] = {o, O, u, U, x, X, c, C, s, S, b, n, p, r};
	int					i;

	i = 0;
	if (format == 'd' || format == 'D' || format == 'i')
		form->type = d;
	else
	{
		while (i < 14 && format != c_set[i])
			i++;
		if (i == 14)
			form->out = &format;
		else
			form->type = t_set[i];
	}
	if (form->type == O || form->type == U || format == 'D')
		form->len = MAX(form->len, l);
	else if ((form->type == format || form->type == s) && form->cl)
		form->type = (form->type == format ? C : S);
}
