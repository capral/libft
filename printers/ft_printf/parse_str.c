/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_str.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:04:41 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		pf_cut_press(t_form *form)
{
	if (form->press > 0 && (int)ft_strlen(form->out) > form->press)
	{
		ft_str_free(&form->out, ft_strsub(form->out, 0, form->press));
	}
	pf_fill_width(form);
}

static void		pf_choose_print(t_form *form, char tmp)
{
	char		str[3];

	ft_bzero(str, 3);
	if (ft_isspace(tmp) && tmp != ' ')
		str[0] = '\\';
	else if (tmp > 0 && tmp < 32)
	{
		str[0] = '^';
		tmp += 64;
	}
	str[1] = tmp;
	form->out = ft_strmake(form->out, str, FIRST);
}

void			pf_parse_non_printable(t_form *form, va_list ap)
{
	char		*ptr;
	char		*tmp;

	if ((ptr = va_arg(ap, char*)))
		form->out = ft_strdup(ptr);
	else
	{
		form->out = ft_strdup("(null)");
		return ;
	}
	pf_cut_press(form);
	tmp = form->out;
	form->out = ft_strnew(0);
	ptr = tmp;
	while (*ptr)
	{
		pf_choose_print(form, *ptr);
		ptr++;
	}
	free(tmp);
}

static void		pf_parse_char(t_form *form, va_list argc)
{
	char		ctmp;

	if (form->type == c || form->type == C)
	{
		if (form->type == C)
			ctmp = (char)va_arg(argc, wchar_t);
		else
			ctmp = va_arg(argc, int);
		form->out = ft_strsub(&ctmp, 0, 1);
	}
	else
		form->out = ft_strsub(form->out, 0, 1);
	pf_fill_char(form);
}

void			pf_parse_str(t_form *form, va_list ap)
{
	char		*ptr;

	if (form->type == S)
		pf_parse_wstr(form, ap);
	else if (form->type == c || form->type == C || form->type < 0)
		pf_parse_char(form, ap);
	else
	{
		if (!(ptr = va_arg(ap, char*)))
			form->out = ft_strdup("(null)");
		else
			form->out = ft_strdup(ptr);
		pf_cut_press(form);
	}
}
