/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   help_int.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:04:34 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

void		pf_fill_quote(t_form *form)
{
	int		i;
	int		j;
	int		k;
	char	*tmp;

	i = (int)ft_strlen(form->out);
	j = (i - 1) / 3;
	tmp = ft_strnew((size_t)(i + j));
	j += --i;
	k = 0;
	while (i > 0)
	{
		if (((k + 1) % 3) == 0)
		{
			tmp[j--] = form->out[i--];
			tmp[j--] = ',';
			k++;
		}
		tmp[j--] = form->out[i--];
		k++;
	}
	tmp[j] = form->out[i];
	ft_str_free(&form->out, tmp);
}

void		pf_fill_hash(t_form *form)
{
	int		mode;
	char	*hash;

	mode = (form->type == x || form->type == X) ? 1 : 0;
	hash = mode ? "0x" : "0";
	if (form->zero)
	{
		form->width -= mode ? 2 : 1;
		pf_fill_width(form);
	}
	if (*form->out == '0' || (form->type != o && form->type != O))
		form->out = ft_strmake(hash, form->out, SECOND);
}

void		pf_fill_width(t_form *form)
{
	int		len;
	char	*filler;

	len = form->width - (int)ft_strlen(form->out);
	if (len > 0)
	{
		filler = ft_strnew(len);
		ft_memset(filler, (form->zero ? '0' : ' '), len);
		if (form->minus)
			form->out = ft_strmake(form->out, filler, BOTH);
		else
			form->out = ft_strmake(filler, form->out, BOTH);
	}
}

void		pf_fill_sign(t_form *form)
{
	char	*sign;

	if (form->zero)
	{
		if (form->width)
			form->width--;
		pf_fill_width(form);
	}
	if (form->sign)
		sign = "-";
	else if (form->plus)
		sign = "+";
	else
		sign = " ";
	form->out = ft_strmake(sign, form->out, SECOND);
}

void		pf_fill_press(t_form *form)
{
	int		len;
	char	*zero_str;

	form->zero = 0;
	len = form->press - (int)ft_strlen(form->out);
	if (len > 0)
	{
		zero_str = ft_strnew(len);
		ft_memset(zero_str, '0', len);
		form->out = ft_strmake(zero_str, form->out, BOTH);
	}
}
