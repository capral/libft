/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse_int.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 16:04:39 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:06:40 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		len_cast_u(t_form *form, va_list args, uintmax_t *ubuff)
{
	if (form->len == hh)
		*ubuff = (unsigned char)va_arg(args, unsigned int);
	else if (form->len == h)
		*ubuff = (unsigned short)va_arg(args, unsigned int);
	else if (form->len == l)
		*ubuff = va_arg(args, unsigned long);
	else if (form->len == ll)
		*ubuff = va_arg(args, unsigned long long);
	else if (form->len == j)
		*ubuff = va_arg(args, uintmax_t);
	else if (form->len == z)
		*ubuff = va_arg(args, size_t);
	else
		*ubuff = va_arg(args, unsigned int);
	if (!*ubuff && form->type != o && form->type != O)
		form->hash = 0;
}

static void		len_cast(t_form *form, va_list args, intmax_t *buff)
{
	if (form->len == hh)
		*buff = (char)va_arg(args, int);
	else if (form->len == h)
		*buff = (short)va_arg(args, int);
	else if (form->len == l)
		*buff = va_arg(args, long);
	else if (form->len == ll)
		*buff = va_arg(args, long long);
	else if (form->len == j)
		*buff = va_arg(args, intmax_t);
	else if (form->len == z)
		*buff = va_arg(args, size_t);
	else
		*buff = va_arg(args, int);
}

void			pf_parse_int_u(t_form *form, va_list ap)
{
	uintmax_t	ubuff;

	len_cast_u(form, ap, &ubuff);
	if (!form->press && !ubuff)
		form->out = ft_strnew(0);
	else if (form->type == b)
		form->out = ft_llitoa_base(ubuff, 2);
	else if (form->type == o || form->type == O)
		form->out = ft_llitoa_base(ubuff, 8);
	else if (form->type == u || form->type == U)
		form->out = ft_llitoa_base(ubuff, 10);
	else if (form->type == x || form->type == X)
		form->out = ft_llitoa_base(ubuff, 16);
	if (form->press >= 0)
		pf_fill_press(form);
	if (form->hash)
		pf_fill_hash(form);
	else
		pf_fill_width(form);
	if (form->type == X)
		ft_str_upper(form->out);
}

void			pf_parse_int(t_form *form, va_list ap)
{
	intmax_t	buff;

	len_cast(form, ap, &buff);
	form->out = (!form->press && !buff) ? ft_strnew(0) : ft_llitoa(buff);
	if (buff < 0)
		form->sign = 1;
	if (form->quote)
		pf_fill_quote(form);
	if (form->press >= 0)
		pf_fill_press(form);
	if (form->sign || form->plus || form->space)
		pf_fill_sign(form);
	else
		pf_fill_width(form);
}
