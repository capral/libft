/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 15:30:21 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:04:29 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

int				ft_dprintf(int fd, const char *format, ...)
{
	va_list		ap;
	int			len;
	char		*ret;

	len = 0;
	if (format && *format && fd >= 0)
	{
		va_start(ap, format);
		len = ft_vsprintf(&ret, format, ap);
		ft_write(fd, ret, len);
		free(ret);
		va_end(ap);
	}
	return (len);
}

int				ft_printf(const char *format, ...)
{
	va_list		ap;
	int			len;
	char		*ret;

	len = 0;
	if (format && *format)
	{
		va_start(ap, format);
		len = ft_vsprintf(&ret, format, ap);
		ft_write(STD_OUT, ret, len);
		free(ret);
		va_end(ap);
	}
	return (len);
}

int				ft_sprintf(char **str, const char *format, ...)
{
	va_list		ap;
	int			len;

	va_start(ap, format);
	len = ft_vsprintf(str, format, ap);
	va_end(ap);
	return (len);
}
