/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_vsprintf.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 12:47:35 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:04:32 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_printf.h"

static void		pf_make_line(t_pf *pf, char **format)
{
	char		*ptr;
	char		*line;

	ptr = (**format == '{' ? *format + 1 : *format);
	while (*ptr && *ptr != '%' && *ptr != '{')
		ptr++;
	line = ft_strsub(*format, 0, ptr - *format);
	pf->size += ptr - *format;
	pf->out = ft_strmake(pf->out, line, BOTH);
	*format = ptr;
}

static void		pf_join_string(t_pf *pf)
{
	if (pf->form->out)
	{
		pf->size += ft_strlen(pf->form->out);
		pf->out = ft_strmake(pf->out, pf->form->out, BOTH);
	}
}

static void		parse_format(t_pf *pf, char **format)
{
	static const t_pffn	fns[4] = {
		pf_parse_int, pf_parse_int_u, pf_parse_str, pf_parse_non_printable
	};

	if (**format == '%')
	{
		pf_parse_percent(pf, format);
		if (pf->form->type == n)
			pf_parse_pointer(pf);
		else
			fns[pf_check_type(pf->form)](pf->form, pf->ap);
		pf_join_string(pf);
	}
	else if (**format == '{')
	{
		if (pf_parse_brackets(pf, format))
			pf_join_string(pf);
		else
			pf_make_line(pf, format);
	}
	else
		pf_make_line(pf, format);
}

static int		ft_vsprintf_inner(char **str, char *format, va_list ap)
{
	t_pf		pf;
	t_form		form;

	ft_bzero(&pf, sizeof(t_pf));
	pf.form = &form;
	va_copy(pf.saved, ap);
	va_copy(pf.ap, ap);
	while (*format)
	{
		ft_bzero(&form, sizeof(t_form));
		form.press = -1;
		form.len = -1;
		form.type = -1;
		parse_format(&pf, &format);
	}
	*str = pf.out;
	va_end(pf.ap);
	return (pf.size);
}

int				ft_vsprintf(char **str, const char *format, va_list ap)
{
	if (!str || !format)
		return (0);
	else if (*format == '\0')
	{
		*str = ft_strnew(0);
		return (0);
	}
	else if (!ft_strchr(format, '%') && !ft_strchr(format, '{'))
	{
		*str = ft_strdup(format);
		return (ft_strlen(*str));
	}
	else
		return (ft_vsprintf_inner(str, (char*)format, ap));
}
