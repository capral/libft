/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 16:13:24 by akaplyar          #+#    #+#             */
/*   Updated: 2016/12/01 18:04:44 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_putnbr_fd(int n, int fd)
{
	int	sign;

	sign = 0;
	if (n == -2147483648)
	{
		n++;
		sign++;
	}
	if (n < 0)
	{
		ft_putchar_fd('-', fd);
		n *= -1;
	}
	if (n > 9)
	{
		ft_putnbr_fd(n / 10, fd);
		(sign ? ft_putnbr_fd(n % 10 + 1, fd) : ft_putnbr_fd(n % 10, fd));
	}
	else
		ft_putchar_fd(n + 48, fd);
}

void	ft_putnbr(int n)
{
	ft_putnbr_fd(n, STD_OUT);
}
