/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_debug.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/06 19:41:13 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:07:45 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "state.h"

t_bool		ft_debug_storage(t_bool state, int mode)
{
	static t_bool	debug;

	if (mode == SET)
		debug = state;
	return (debug);
}

int			ft_debug(const char *format, ...)
{
	va_list		ap;
	int			len;
	char		*ret;

	len = 0;
	if (ft_debug_storage(FALSE, GET) && format && *format)
	{
		va_start(ap, format);
		len = ft_vsprintf(&ret, format, ap);
		ft_write(STD_ERR, ret, len);
		free(ret);
		va_end(ap);
	}
	return (len);
}
