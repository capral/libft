/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_exit.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alex <alex@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/14 18:15:10 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/10 00:38:34 by alex             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "state.h"

static void	print_err_message(const char *msg)
{
	ft_putstr_fd(GET_PROJECT(), STD_ERR);
	ft_putstr_fd(": ", STD_ERR);
	ft_putendl_fd(msg, STD_ERR);
}

void		ft_exit_msg(const char *msg)
{
	print_err_message(msg);
	exit(FAIL);
}

void		ft_exit(const int errno)
{
	if (errno == ENOMEM)
		print_err_message("cannot allocate memory");
	else if (errno == EBADWRITE)
		print_err_message("error while trying to write");
	else if (errno == ENOERR)
		print_err_message(" good bye)");
	exit(errno);
}
