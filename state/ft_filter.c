/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_filter.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/14 18:15:10 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:07:48 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "state.h"

t_bool		ft_char_filter(char c)
{
	char	comp;

	GET_CHAR(comp);
	return (comp == c);
}

t_bool		ft_str_filter(char *str)
{
	char	*comp;

	GET_STR(comp);
	return (ft_strequ(comp, str));
}
