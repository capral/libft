/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_handle_storage.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/14 18:15:10 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:07:50 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "state.h"

void			ft_handle_storage(void **data, char *c, char **str, int mode)
{
	static void	*saved_data = NULL;
	static char	saved_char = '\0';
	static char	*saved_str = NULL;

	if (mode & SET)
	{
		if (mode & S_DATA)
			ft_ptr_free(&saved_data, *data);
		if (mode & S_CHAR)
			saved_char = *c;
		if (mode & S_STR)
			ft_str_free(&saved_str, *str);
	}
	else
	{
		if (mode & S_DATA)
			*data = saved_data;
		if (mode & S_CHAR)
			*c = saved_char;
		if (mode & S_STR)
			*str = saved_str;
	}
}

char			*ft_handle_project(char *name, int mode)
{
	static char	*project_name = NULL;

	if (mode == SET)
		project_name = name;
	return (project_name);
}
