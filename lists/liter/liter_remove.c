/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_remove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:30 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			liter_remove_fn(t_liter *iter, t_pred_fn1 f, t_nop_fn1 g)
{
	if (iter && f)
	{
		while (iter->this->next)
		{
			if (f(iter->this->data))
				liter_del_fn(iter, g);
			else
			{
				iter->prev = iter->this;
				iter->this = iter->this->next;
			}
		}
		if (iter->this && f(iter->this->data))
			liter_del_fn(iter, g);
	}
}

void			liter_remove(t_liter *iter, t_pred_fn1 f)
{
	liter_remove_fn(iter, f, NULL);
}
