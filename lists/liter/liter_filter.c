/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_filter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 15:11:36 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:14 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

static t_list		*liter_filter_storage(void *data, t_pred_fn1 f,
														t_fn1 g, int mode)
{
	static t_liter		iter = {NULL, NULL};
	static t_fn1		fn = NULL;
	static t_pred_fn1	pred = NULL;

	if (mode == SET)
	{
		iter.prev = NULL;
		iter.this = NULL;
		pred = f;
		fn = g;
	}
	else if (mode == GET)
		return (iter.this);
	else
	{
		if (pred(data))
			liter_push(&iter, list_new(fn(data)));
	}
	return (NULL);
}

static void			liter_filter_lambda(void *data)
{
	liter_filter_storage(data, NULL, NULL, NONE);
}

t_list				*liter_filter_cpy(t_liter *iter, t_pred_fn1 f, t_fn1 g)
{
	t_list			*list;

	list = NULL;
	if (iter && iter->this && f && g)
	{
		liter_filter_storage(NULL, f, g, SET);
		liter_iter(iter, liter_filter_lambda);
		list = liter_filter_storage(NULL, NULL, NULL, GET);
	}
	return (list);
}

t_list				*liter_filter(t_liter *iter, t_pred_fn1 f)
{
	return (liter_filter_cpy(iter, f, idfn));
}
