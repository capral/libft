/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:26 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

static t_list		*liter_map_storage(void *data, t_fn1 f, int mode)
{
	static t_liter	iter = {NULL, NULL};
	static t_fn1	fn = NULL;

	if (mode == SET)
	{
		iter.prev = NULL;
		iter.this = NULL;
		fn = f;
	}
	else if (mode == GET)
		return (iter.this);
	else
		liter_push(&iter, list_new(fn(data)));
	return (NULL);
}

static void			liter_map_lambda(void *data)
{
	liter_map_storage(data, NULL, NONE);
}

t_list				*liter_map(t_liter *iter, t_fn1 f)
{
	t_list			*list;

	list = NULL;
	if (iter && iter->this && f)
	{
		liter_map_storage(NULL, f, SET);
		liter_iter(iter, liter_map_lambda);
		list = liter_map_storage(NULL, NULL, GET);
	}
	return (list);
}
