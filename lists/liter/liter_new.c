/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:28 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_liter			*liter_new(void)
{
	t_liter		*iter;

	iter = (t_liter*)ft_memalloc(sizeof(t_liter));
	return (iter);
}

t_liter			*liter_new_init(t_list *list)
{
	t_liter		*iter;

	iter = liter_new();
	iter->this = list;
	return (iter);
}
