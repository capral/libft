/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_iter.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:21 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		liter_iter(t_liter *iter, t_nop_fn1 f)
{
	if (iter && f)
	{
		while (iter->this->next)
		{
			f(iter->this->data);
			iter->prev = iter->this;
			iter->this = iter->this->next;
		}
		if (iter->this)
			f(iter->this->data);
	}
}
