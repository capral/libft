/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_find.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:16 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_liter			*liter_find(t_liter *iter, t_pred_fn1 f)
{
	if (iter && f)
	{
		while (iter->this && !f(iter->this->data))
		{
			iter->prev = iter->this;
			iter->this = iter->this->next;
		}
	}
	return (iter);
}

t_liter			*liter_find_new(t_list *list, t_pred_fn1 f)
{
	t_liter		*iter;

	if (list && f)
	{
		iter = liter_new_init(list);
		return (liter_find(iter, f));
	}
	else
		return (NULL);
}
