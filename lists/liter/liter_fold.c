/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_fold.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 11:38:38 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:18 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*liter_fold(t_liter *iter, void *acc, t_fn2 f)
{
	if (iter && iter->this && f)
	{
		while (iter->this->next)
		{
			acc = f(acc, iter->this->data);
			iter->prev = iter->this;
			iter->this = iter->this->next;
		}
		if (iter->this)
			acc = f(acc, iter->this->data);
	}
	return (acc);
}
