/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_del.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:12 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		liter_free(t_liter **iter)
{
	ft_ptr_free((void**)iter, NULL);
}

void		liter_del_fn(t_liter *iter, t_nop_fn1 f)
{
	if (iter && iter->this)
	{
		if (iter->prev)
			iter->prev->next = iter->this->next;
		list_del1_fn(&iter->this, f);
		iter->this = iter->prev->next;
	}
}

void		liter_delr_fn(t_liter *iter, t_list **root, t_nop_fn1 f)
{
	if (iter && iter->this)
	{
		if (iter->prev)
			iter->prev->next = iter->this->next;
		if (root && *root == iter->this)
			*root = iter->this->next;
		list_del1_fn(&iter->this, f);
	}
}

void		liter_del(t_liter *iter)
{
	liter_del_fn(iter, free);
}

void		liter_delr(t_liter *iter, t_list **root)
{
	liter_delr_fn(iter, root, free);
}
