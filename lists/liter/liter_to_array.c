/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_to_array.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 15:35:52 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:32 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		**liter_to_array_fn(t_liter *iter, t_fn1 f)
{
	if (iter)
		return (list_to_array_fn(iter->this, f));
	else
		return (NULL);
}

void		**liter_to_array(t_liter *iter)
{
	return (liter_to_array_fn(iter, idfn));
}
