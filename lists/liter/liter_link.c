/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   liter_link.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:23 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		liter_add(t_liter *iter, t_list *new_node)
{
	if (iter && new_node)
	{
		if (iter->this)
		{
			new_node->next = iter->this;
			iter->this = new_node;
		}
		else
		{
			iter->this = new_node;
		}
		if (iter->prev)
			iter->prev->next = new_node;
	}
}

void		liter_push(t_liter *iter, t_list *new_node)
{
	if (iter && new_node)
	{
		if (iter->this)
		{
			iter->this->next = new_node;
			iter->this = new_node;
		}
		else
		{
			iter->this = new_node;
		}
		if (iter->prev)
			iter->prev->next = new_node;
	}
}
