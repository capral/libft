/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlist_filter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 15:11:40 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/09 18:23:39 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

static t_list		*liter_filter_storage(void *data, t_pred_fn1 f,
														t_fn1 g, int mode)
{
	static t_liter		iter = {NULL, NULL};
	static t_fn1		fn = NULL;
	static t_pred_fn1	pred = NULL;

	if (mode == SET)
	{
		iter.prev = NULL;
		iter.this = NULL;
		pred = f;
		fn = g;
	}
	else if (mode == GET)
		return (iter.this);
	else
	{
		if (pred(data))
			liter_push(&iter, list_new(fn(data)));
	}
	return (NULL);
}

t_dlist			*dlist_filter(t_dlist *dlist, t_pred_fn1 f)
{
	t_dlist		*ptr;

	ptr = NULL;
	if (dlist && f)
		ptr = dlist_fold((void*)dlist, f);
	return (ptr);
}

t_dlist			*dlist_filter_cpy(t_dlist *dlist, t_pred_fn1 f, t_fn1 g)
{
	t_liter		iter;
	t_dlist		*ptr;

	ptr = NULL;
	if (dlist && f && g)
	{
		iter.this = dlist;
		ptr = liter_filter_cpy(&iter, f, g);
	}
	return (ptr);
}
