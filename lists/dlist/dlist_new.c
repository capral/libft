/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlist_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 16:31:20 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/09 16:33:45 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include "list.h"

t_dlist			*dlist_new(void *data)
{
	t_dlist		*node;

	node = (t_dlist*)ft_malloc(sizeof(t_dlist));
	node->data = data;
	node->next = NULL;
	node->prev = NULL;
	return (node);
}

t_dlist			*dlist_new_cpy(void const *data, size_t size)
{
	t_dlist		*node;

	node = (t_dlist*)ft_memalloc(sizeof(t_dlist));
	if (data)
		node->data = ft_memcpy(ft_malloc(size), data, size);
	return (node);
}
