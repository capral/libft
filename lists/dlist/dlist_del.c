/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlist_del.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/09 16:30:52 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		dlist_del1_fn(t_dlist **root, t_nop_fn1 f)
{
	t_dlist	*tmp;

	if (root && *root)
	{
		tmp = *root;
		if (tmp->data)
		{
			if (f)
				f(tmp->data);
			else
				free(tmp->data);
		}
		free(tmp);
		*root = NULL;
	}
}

void		dlist_del_fn(t_dlist **root, t_nop_fn1 f)
{
	t_dlist	*list;
	t_dlist	*tmp;

	if (root)
	{
		list = *root;
		if (!f)
			f = free;
		while (list)
		{
			tmp = list;
			list = list->next;
			if (tmp->data)
				f(tmp->data);
			free(tmp);
		}
		*root = NULL;
	}
}

void		dlist_del1(t_dlist **root)
{
	dlist_del1_fn(root, free);
}

void		dlist_del(t_dlist **root)
{
	dlist_del_fn(root, free);
}
