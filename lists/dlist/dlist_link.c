/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlist_link.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alex <alex@student.unit.ua>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/10 20:53:45 by alex             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		dlist_add(t_dlist **root, t_dlist *new_node)
{
	t_dlist	*dlist;

	if (root && new_node)
	{
		if (*root)
		{
			new_node->next = *root;
			*root->prev = new_node;
			*root = new_node;
		}
		else
			*root = new_node;
	}
}

void		dlist_push(t_dlist **root, t_dlist *new_node)
{
	t_dlist	*dlist;

	if (root)
	{
		if (!*root)
			*root = new_node;
		else
		{
			dlist = *root;
			while (dlist->next)
				dlist = dlist->next;
			dlist->next = new_node;
			new_node->prev = dlist;
		}
	}
}

void		dlist_add_data(t_dlist *dlist, void *data)
{
	dlist_add(dlist, dlist_new(data));
}

void		dlist_push_data(t_dlist *dlist, void *data)
{
	dlist_push(dlist, dlist_new(data));
}
