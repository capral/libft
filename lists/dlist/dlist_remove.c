/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlist_remove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/09 16:34:35 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			dlist_remove_fn(t_dlist *dlist, t_pred_fn1 f, t_nop_fn1 g)
{
	t_liter		iter;

	if (dlist && f)
	{
		iter.this = dlist;
		liter_remove_fn(&iter, f, g);
	}
}

void			dlist_remove(t_dlist *dlist, t_pred_fn1 f)
{
	dlist_remove_fn(dlist, f, NULL);
}
