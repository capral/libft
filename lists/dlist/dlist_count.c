/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   dlist_count.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/09 16:26:36 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/09 16:34:42 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

size_t		dlist_count(t_dlist *dlist)
{
	size_t	count;

	count = 0;
	if (dlist)
	{
		count = 1;
		while (dlist->next)
		{
			count++;
			dlist = dlist->next;
		}
	}
	return (count);
}
