/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_link.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:27 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		list_add(t_list **root, t_list *new_node)
{
	if (root && new_node)
	{
		if (*root)
		{
			new_node->next = *root;
			*root = new_node;
		}
		else
			*root = new_node;
	}
}

void		list_push(t_list **root, t_list *new_node)
{
	t_list	*list;

	if (root)
	{
		if (!*root)
			*root = new_node;
		else
		{
			list = *root;
			while (list->next)
				list = list->next;
			list->next = new_node;
		}
	}
}
