/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_to_array.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 15:35:15 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:44 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		**list_to_array_fn(t_list *list, t_fn1 f)
{
	size_t	i;
	size_t	len;
	void	**arr;

	arr = NULL;
	if (list)
	{
		i = 0;
		len = list_count(list);
		arr = (void*)ft_malloc(sizeof(void*) * (len + 1));
		while (i < len)
		{
			arr[i++] = f(list->data);
			list = list->next;
		}
		arr[i] = NULL;
	}
	return (arr);
}

void		**list_to_array(t_list *list)
{
	return (list_to_array_fn(list, idfn));
}
