/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_filter.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 15:11:40 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:18 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_list			*list_filter(t_list *list, t_pred_fn1 f)
{
	t_liter		iter;
	t_list		*ptr;

	ptr = NULL;
	if (list && f)
	{
		iter.this = list;
		ptr = liter_filter(&iter, f);
	}
	return (ptr);
}

t_list			*list_filter_cpy(t_list *list, t_pred_fn1 f, t_fn1 g)
{
	t_liter		iter;
	t_list		*ptr;

	ptr = NULL;
	if (list && f && g)
	{
		iter.this = list;
		ptr = liter_filter_cpy(&iter, f, g);
	}
	return (ptr);
}
