/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_iter.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:25 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			list_iter(t_list *list, t_nop_fn1 f)
{
	if (f)
	{
		while (list)
		{
			f(list->data);
			list = list->next;
		}
	}
}
