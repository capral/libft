/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_new.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:36 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_list			*list_new(void *data)
{
	t_list		*node;

	node = (t_list*)ft_malloc(sizeof(t_list));
	node->data = data;
	node->next = NULL;
	return (node);
}

t_list			*list_new_cpy(void const *data, size_t size)
{
	t_list		*node;

	node = (t_list*)ft_memalloc(sizeof(t_list));
	if (data)
		node->data = ft_memcpy(ft_malloc(size), data, size);
	return (node);
}
