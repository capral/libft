/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_remove.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:38 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			list_remove_fn(t_list *list, t_pred_fn1 f, t_nop_fn1 g)
{
	t_liter		iter;

	if (list && f)
	{
		iter.this = list;
		liter_remove_fn(&iter, f, g);
	}
}

void			list_remove(t_list *list, t_pred_fn1 f)
{
	list_remove_fn(list, f, NULL);
}
