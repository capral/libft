/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   list_del.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:00:16 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		list_del1_fn(t_list **root, t_nop_fn1 f)
{
	if (root && *root)
	{
		if ((*root)->data)
		{
			if (f)
				f((*root)->data);
			else
				free((*root)->data);
		}
		free(*root);
		*root = NULL;
	}
}

void		list_del_fn(t_list **root, t_nop_fn1 f)
{
	t_list	*list;
	t_list	*tmp;

	if (root)
	{
		list = *root;
		if (!f)
			f = free;
		while (list)
		{
			tmp = list;
			list = list->next;
			if (tmp->data)
				f(tmp->data);
			free(tmp);
		}
		*root = NULL;
	}
}

void		list_del1(t_list **root)
{
	list_del1_fn(root, free);
}

void		list_del(t_list **root)
{
	list_del_fn(root, free);
}
