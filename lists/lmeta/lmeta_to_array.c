/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_to_array.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 11:38:38 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:42 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		**lmeta_to_array_fn(t_lmeta *meta, t_fn1 f)
{
	int		i;
	void	**arr;
	t_list	*ptr;

	arr = NULL;
	if (meta && meta->count > 0)
	{
		i = 0;
		arr = (void**)ft_malloc(sizeof(void*) * (meta->count + 1));
		ptr = meta->first;
		while (i < meta->count)
		{
			arr[i++] = f(ptr->data);
			ptr = ptr->next;
		}
		arr[i] = NULL;
	}
	return (arr);
}

char		**lmeta_to_char_array(t_lmeta *meta)
{
	int		i;
	char	**arr;
	t_list	*ptr;

	arr = NULL;
	if (meta && meta->count > 0)
	{
		i = 0;
		arr = (char**)ft_malloc(sizeof(char*) * (meta->count + 1));
		ptr = meta->first;
		while (i < meta->count)
		{
			arr[i++] = (char*)ptr->data;
			ptr = ptr->next;
		}
		arr[i] = NULL;
	}
	return (arr);
}

void		**lmeta_to_array(t_lmeta *meta)
{
	return (lmeta_to_array_fn(meta, idfn));
}
