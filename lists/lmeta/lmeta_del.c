/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_del.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:01:52 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void		lmeta_free(t_lmeta **meta, t_nop_fn1 f, t_bool all)
{
	if (all)
		list_del_fn(&(*meta)->first, f);
	liter_free(&(*meta)->iter);
	ft_ptr_free((void**)meta, NULL);
}

void		lmeta_del_fn(t_lmeta *meta, t_nop_fn1 f)
{
	if (meta && meta->count > 0)
	{
		if (meta->iter && meta->iter->this)
		{
			if (meta->last == meta->iter->this)
				meta->last = meta->iter->prev;
			meta->count--;
			liter_delr_fn(meta->iter, &meta->first, f);
		}
	}
}

void		lmeta_del(t_lmeta *meta)
{
	lmeta_del_fn(meta, free);
}
