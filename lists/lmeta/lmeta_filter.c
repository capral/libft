/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_filter.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:22 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

static void			lmeta_filter_storage(void *data, t_pred_fn1 f,
														t_fn1 g, int mode)
{
	static t_lmeta		*meta;
	static t_fn1		fn = NULL;
	static t_pred_fn1	pred = NULL;

	if (mode == SET)
	{
		meta = (t_lmeta*)data;
		pred = f;
		fn = g;
	}
	else if (mode == NONE)
	{
		if (pred(data))
			lmeta_push(meta, list_new(fn(data)));
	}
}

static void			lmeta_filter_lambda(void *data)
{
	lmeta_filter_storage(data, NULL, NULL, NONE);
}

t_lmeta				*lmeta_filter_cpy(t_lmeta *meta, t_pred_fn1 f, t_fn1 g)
{
	t_lmeta			*new_meta;

	new_meta = NULL;
	if (meta && f)
	{
		new_meta = lmeta_new();
		lmeta_filter_storage(new_meta, f, g, SET);
		lmeta_iter(meta, lmeta_filter_lambda);
		lmeta_filter_storage(NULL, NULL, NULL, GET);
	}
	return (new_meta);
}

t_lmeta				*lmeta_filter(t_lmeta *meta, t_pred_fn1 f)
{
	return (lmeta_filter_cpy(meta, f, idfn));
}
