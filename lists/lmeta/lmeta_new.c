/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_new.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:36 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_lmeta			*lmeta_new(void)
{
	t_lmeta		*meta;

	meta = (t_lmeta*)ft_memalloc(sizeof(t_lmeta));
	meta->iter = liter_new();
	return (meta);
}

t_lmeta			*lmeta_new_init(t_list *list)
{
	t_lmeta		*meta;

	meta = lmeta_new();
	meta->first = list;
	meta->last = list;
	meta->iter->this = list;
	meta->count = 1;
	return (meta);
}
