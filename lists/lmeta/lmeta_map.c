/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_map.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:34 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

static void			lmeta_map_storage(void *data, t_fn1 f, int mode)
{
	static t_lmeta	*meta;
	static t_fn1	fn = NULL;

	if (mode == SET)
	{
		meta = (t_lmeta*)data;
		fn = f;
	}
	else if (mode == NONE)
		lmeta_push(meta, list_new(fn(data)));
}

static void			lmeta_map_lambda(void *data)
{
	lmeta_map_storage(data, NULL, NONE);
}

t_lmeta				*lmeta_map(t_lmeta *meta, t_fn1 f)
{
	t_lmeta			*new_meta;

	new_meta = NULL;
	if (meta && f)
	{
		new_meta = lmeta_new();
		lmeta_map_storage(new_meta, f, SET);
		lmeta_iter(meta, lmeta_map_lambda);
		lmeta_map_storage(NULL, NULL, GET);
	}
	return (new_meta);
}
