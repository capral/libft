/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_remove.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:40 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void			lmeta_remove_fn(t_lmeta *meta, t_pred_fn1 f, t_nop_fn1 g)
{
	t_liter		*iter;

	if (meta && f)
	{
		if (!meta->iter)
			meta->iter = liter_new();
		meta->iter->prev = NULL;
		meta->iter->this = meta->first;
		iter = meta->iter;
		while (iter->this->next)
		{
			if (f(iter->this->data))
				lmeta_del_fn(meta, g);
			else
			{
				iter->prev = iter->this;
				iter->this = iter->this->next;
			}
		}
		if (iter->this && f(iter->this->data))
			lmeta_del_fn(meta, g);
	}
}

void			lmeta_remove(t_lmeta *meta, t_pred_fn1 f)
{
	lmeta_remove_fn(meta, f, NULL);
}
