/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_find.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/03/08 16:02:05 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:18 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

t_liter			*lmeta_find(t_lmeta *meta, t_pred_fn1 f)
{
	if (meta && meta->iter)
	{
		meta->iter->this = meta->first;
		liter_find(meta->iter, f);
	}
	return (meta->iter);
}
