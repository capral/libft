/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_fold.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 11:38:38 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:16 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		*lmeta_fold(t_lmeta *meta, void *acc, t_fn2 f)
{
	if (meta && meta->iter)
	{
		meta->iter->this = meta->first;
		acc = liter_fold(meta->iter, acc, f);
	}
	return (acc);
}
