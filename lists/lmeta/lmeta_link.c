/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   lmeta_link.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/23 11:38:38 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:02:11 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "list.h"

void	lmeta_add(t_lmeta *meta, t_list *new_node)
{
	if (meta && new_node)
	{
		list_add(&meta->first, new_node);
		if (meta->count == 0)
			meta->last = new_node;
		meta->count++;
	}
}

void	lmeta_push(t_lmeta *meta, t_list *new_node)
{
	if (meta && new_node)
	{
		if (!meta->first)
			lmeta_add(meta, new_node);
		else
		{
			meta->last->next = new_node;
			meta->last = new_node;
			meta->count++;
		}
	}
}

void	lmeta_add_data(t_lmeta *meta, void *data)
{
	lmeta_add(meta, list_new(data));
}

void	lmeta_push_data(t_lmeta *meta, void *data)
{
	lmeta_push(meta, list_new(data));
}
