/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_readfile.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:33 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 16:07:26 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char		*ft_readfile(const int fd)
{
	ssize_t	ret;
	char	*buff;
	char	*str;

	str = ft_strnew(1);
	buff = ft_strnew(BUFF_SIZE + 1);
	while ((ret = read(fd, buff, BUFF_SIZE)))
	{
		if (ret < 0)
		{
			ft_str_free(&str, NULL);
			ft_str_free(&buff, NULL);
			return (NULL);
		}
		buff[ret] = '\0';
		str = ft_strmake(str, buff, FIRST);
	}
	ft_str_free(&buff, NULL);
	return (str);
}
