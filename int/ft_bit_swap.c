/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_bit_swap.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:59:50 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "int.h"

unsigned long long	ft_bit_swap_ll(unsigned long long llu)
{
	return (llu >> 56 | (llu >> 40 & 0xff00) |
			(llu >> 24 & 0xff0000) | (llu >> 8 & 0xff000000) |
			(llu << 8 & 0xff00000000) | (llu << 24 & 0xff0000000000) |
			(llu << 40 & 0xff000000000000) | llu << 56);
}

unsigned			ft_bit_swap(unsigned u)
{
	return (u >> 24 | (u >> 8 & 0xff00) | (u << 8 & 0xff0000) | u << 24);
}
