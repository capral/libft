/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_intlen.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:59:55 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "int.h"

int			ft_intlen(long long num)
{
	int		len;

	len = 1;
	if (num < 0)
		len++;
	while (num > 9 || num < -9)
	{
		num /= 10;
		len++;
	}
	return (len);
}
