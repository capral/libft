/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/16 12:28:32 by akaplyar          #+#    #+#             */
/*   Updated: 2019/03/08 15:59:46 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "int.h"

int		ft_atoi(const char *str)
{
	int	i;
	int	nbr;

	nbr = 0;
	i = 1;
	while (ft_isspace(*str))
		str++;
	if (*str == '-' || *str == '+')
	{
		i = (*str == '-' ? -1 : 1);
		str++;
	}
	while (*str && *str >= 48 && *str <= 57)
		nbr = (nbr * 10) + (*str++ - 48);
	return (nbr * i);
}
