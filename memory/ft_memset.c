/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   memset.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/28 10:32:46 by akaplyar          #+#    #+#             */
/*   Updated: 2016/12/08 17:16:00 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory.h"

void	*ft_memset(void *ptr, int c, size_t len)
{
	char	*mem;

	mem = (char*)ptr;
	while (len > 0)
	{
		*mem = (char)c;
		mem++;
		len--;
	}
	return (ptr);
}
