/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_memalloc.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/30 10:36:13 by akaplyar          #+#    #+#             */
/*   Updated: 2019/02/10 17:24:58 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "memory.h"

void		*ft_memalloc(size_t size)
{
	void	*ptr;

	ptr = ft_malloc(size);
	ft_bzero(ptr, size);
	return (ptr);
}
