/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_matrix_del.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: akaplyar <akaplyar@student.unit.ua>        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/30 16:31:27 by akaplyar          #+#    #+#             */
/*   Updated: 2019/02/10 18:20:10 by akaplyar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void	ft_matrix_del(void **matrix, int count)
{
	int i;

	i = 0;
	if (count < 0)
		while (matrix[i])
			free(matrix[i++]);
	else
		while (i < count)
			free(matrix[i++]);
	free(matrix);
}
